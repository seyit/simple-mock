from bottle import route, run, response, request
from utils import parse_config_yaml_to_json, handle_request


def generate(configfile):
    config = parse_config_yaml_to_json(configfile)
    endpoints = config['endpoints']

    # @route("/<path:re:.*>", method=["GET", "POST"])
    for r in [j['route'] for j in endpoints]:
        @route(r, method=["GET", "POST"])
        def index(*args, **kwargs):
            resp = handle_request(request, endpoints, *args, **kwargs)
            return resp


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--config', dest='configfile', required=True, help='config.yml')
    parser.add_argument('--port', dest='port', default="5000", required=False, help='port')
    args = parser.parse_args()

    print(f"Running {args.configfile} on {args.port}")
    generate(args.configfile)
    run(host='localhost', port=args.port)


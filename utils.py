import json
import random
import string

import yaml
from bottle import response


def return_exception(message):
    raise Exception(message)


def parse_config_yaml_to_json(configfile):
    with open(configfile, "r") as yaml_doc:
        yaml_to_dict = yaml.load(yaml_doc, Loader=yaml.FullLoader)
        return dict(yaml_to_dict)


def check_input(params, input_format):
    """
    :param params: request.params
    :param input_format: {'key1': {'type': 'str', 'required': True},
                          'key2': {'type': 'str', 'required': True},
                          'key3': {'type': 'int', 'required': False}}
    :return: none
    """

    if input_format is None:
        return

    required_keys = [i for i, j in input_format.items() if j.get('required')]
    url_keys = params.keys()
    missing_keys = [i for i in required_keys if i not in url_keys]

    if len(missing_keys):
        return_exception(f"missing key: {missing_keys}")

    #TODO: Add type check also.


def generate_string(min=0, max=50):
    return ''.join(random.choice(string.ascii_lowercase + string.digits) for i in range(random.choice(range(min, max+1))))


def generate_integer(min=0, max=4):
    return random.choice(range(min, max+1))


def generate_random_output(output_format=None):
    """
    :param output_format: {'outkey1': {'type': 'int', 'min': 0, 'max': 1000},
                           'outkey2': {'type': 'str', 'min': 0, 'max': 50},
                           'outkey3': {'type': 'int', 'value': 50}}
    :return: {'outkey1': 994,
              'outkey2': "v9wvzjhwwoiovsj76mh0",
              'outkey3': 50}
    """

    if output_format is None:
        return {"msg": "success"}

    type_functions = {
        "str": generate_string,
        "int": generate_integer,
    }

    resp = {}
    for k, content in output_format.items():
        value = content.get('value')
        if value or type(value) == list:
            resp[k] = value
        else:
            resp[k] = type_functions[content['type']](content.get('min'), content.get('max'))

    return resp


def get_file_output(output_file):
    with open(output_file, "r") as flh:
        return json.loads(flh.read())


def handle_request(request, endpoints, *args, **kwargs):
    print(request.route.rule)
    urls = {j['route']: j for j in endpoints}
    resp = generate_random_output()

    methods = urls.get(request.route.rule, {}).get('methods')

    if not methods:
        return_exception("url not found")

    if request.method in ['GET', 'POST']:

        method = methods.get('get')
        if not method:
            return_exception("method not found")

        defaults = method.get('defaults')
        if defaults is not None:

            check_input(request.params, defaults.get('input_format'))

            if defaults.get('output_file'):
                resp = get_file_output(defaults['output_file'])

            if defaults.get('output_format'):
                resp = generate_random_output(defaults['output_format'])

            # cases = method.get('cases')

    response.status = 200
    response.mimetype = 'application/json'

    return resp
